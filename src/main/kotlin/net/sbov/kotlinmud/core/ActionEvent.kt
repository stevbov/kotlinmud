package net.sbov.kotlinmud.core

interface ActionEvent: Event {
    val entity: Entity
    val message: String
}
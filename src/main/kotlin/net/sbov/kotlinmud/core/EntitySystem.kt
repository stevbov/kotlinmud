package net.sbov.kotlinmud.core

import org.slf4j.LoggerFactory

abstract class EntitySystem(val handlesMessages: Array<Class<out Event>> = arrayOf()) {
    fun update(game: Game) {
        onUpdate(game)
    }

    abstract fun onUpdate(game: Game)
    abstract fun onEvent(game: Game, event: Event): Boolean

    protected val log = LoggerFactory.getLogger(this.javaClass)
}
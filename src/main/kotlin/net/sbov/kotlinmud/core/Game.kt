package net.sbov.kotlinmud.core

import org.slf4j.LoggerFactory
import java.util.concurrent.ConcurrentLinkedDeque

class Game {
    var tick: Int = 0
        private set
    private val systems = ArrayList<EntitySystem>()
    private val eventHandlers = HashMap<Class<out Event>, EntitySystem>()
    private val log = LoggerFactory.getLogger(this.javaClass)
    val entities = ArrayList<Entity>()
    private val events = ConcurrentLinkedDeque<Event>()
    private var runThread: Thread? = null

    fun run() {
        runThread = Thread.currentThread()
        while (true) {
            val start = System.currentTimeMillis()

            drainEvents()
            for (system in systems) {
                system.update(this)
            }

            val wait = 50 - (System.currentTimeMillis() - start)
            if (wait > 0) {
                log.debug("Tick $tick sleeping for ${wait}ms")
                Thread.sleep(wait)
                ++tick
            }
        }
    }

    private fun drainEvents() {
        while (true) {
            val event = events.poll() ?: break
            dispatchEvent(event)
        }
    }

    fun queueEvent(event: Event) {
        events.add(event)
    }

    fun sendEvent(event: Event): Boolean {
        if (runThread != Thread.currentThread()) {
            throw IllegalStateException("Cannot send events off thread, use queueEvent instead")
        }
        return dispatchEvent(event)
    }

    private fun dispatchEvent(event: Event): Boolean {
        val handler = eventHandlers[event.javaClass]
        if (handler != null) {
            if (handler.onEvent(this, event)) {
                return true
            }
        }

        for (system in systems) {
            if (system.onEvent(this, event)) {
                return true
            }
        }

        return false
    }

    fun addSystem(system: EntitySystem) {
        systems.add(system)
        for (message in system.handlesMessages) {
            val existingHandler = eventHandlers[message]
            if (existingHandler != null) {
                throw IllegalStateException("Message ${message.javaClass} already has handler ${existingHandler.javaClass}, cannot set handler ${system.javaClass}")
            }
            eventHandlers[message] = system
        }
    }

    fun newEntity(): Entity {
        val e = Entity()
        entities.add(e)
        return e
    }
}
package net.sbov.kotlinmud.core

import java.util.*

class Entity {
    val id: UUID = UUID.randomUUID()
    private val mutableComponents: MutableMap<Class<out Component>, Component> = mutableMapOf()
    val components: Map<Class<out Component>, Component> = Collections.unmodifiableMap(mutableComponents)

    fun addComponent(component: Component) {
        mutableComponents[component::class.java] = component
        component.entity = this
    }

    inline fun <reified T: Component> component(): T? {
        val component = components[T::class.java] ?: return null
        if (component !is T) {
            throw IllegalStateException("Component on entity $id for class ${T::class.java} was class ${component::class.java}")
        }
        return component
    }

    fun removeComponent(component: Component) {
        if (component.entity != this) {
            if (component.entity == null) {
                throw IllegalArgumentException("Entity $id - removing component ${component::class.java} had null entity")
            } else {
                throw IllegalArgumentException("Entity $id - removing component ${component::class.java} had entity ${component.entity!!.id}")
            }
        }
        mutableComponents.remove(component::class.java)
        component.entity = null
    }
}
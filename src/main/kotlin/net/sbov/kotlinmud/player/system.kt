package net.sbov.kotlinmud.player

import net.sbov.kotlinmud.chat.ChatComponent
import net.sbov.kotlinmud.command.CommandComponent
import net.sbov.kotlinmud.command.CommandEvent
import net.sbov.kotlinmud.command.UnhandledCommandEvent
import net.sbov.kotlinmud.command.command
import net.sbov.kotlinmud.core.*
import net.sbov.kotlinmud.describe.DescribeComponent
import net.sbov.kotlinmud.describe.validateName

class PlayerSystem : EntitySystem(
        arrayOf(PlayerInputEvent::class.java,
                PlayerDisconnectEvent::class.java,
                PlayerConnectEvent::class.java)) {
    private val playerToEntity = HashMap<Player, Entity>()
    private val entityToPlayer = HashMap<Entity, Player>()

    override fun onUpdate(game: Game) {
        processOutput()
    }

    private fun processOutput() {
        for ((player, entity) in playerToEntity) {
            val comp = entity.player()!!
            val output = comp.output
            comp.output = ""
            if (!output.isBlank()) {
                player.write(output)
                player.write("\r\n${comp.prompt} ")
            }
        }
    }

    override fun onEvent(game: Game, event: Event): Boolean {
        when (event) {
            is PlayerInputEvent -> {
                playerToEntity[event.player]!!.command()!!.inputs.add(event.msg)
                return true
            }
            is PlayerConnectEvent -> {
                log.info("Connect From ${event.player.ip}")
                val entity = newPlayer(game, event)
                playerToEntity[event.player] = entity
                entityToPlayer[entity] = event.player
                entity.player()!!.prompt = "Please enter your name:"
                entity.player()!!.writeLine("Welcome to the echo player!")
                return true
            }
            is PlayerDisconnectEvent -> {
                log.info("Disconnect From ${event.player.ip}")
                playerToEntity.remove(event.player)
                return true
            }
            is ActionEvent -> {
                val comp = event.entity.player() ?: return false
                comp.writeLine(event.message)
                return true
            }
            is UnhandledCommandEvent -> {
                val comp = event.cmd.entity.player() ?: return false
                comp.writeLine("Huh!?")
                return true
            }
            is CommandEvent -> {
                val entity = event.entity
                val player = entityToPlayer[event.entity] ?: return false
                val comp = entity.player()!!

                if (event.original.toLowerCase() == "quit") {
                    player.disconnect()
                    return true
                } else {
                    when (comp.state) {
                        PlayerState.LOGIN -> {
                            val err = validateName(event.original)
                            if (err != null) {
                                comp.writeLine(err)
                            } else {
                                comp.entity!!.addComponent(DescribeComponent(event.original))
                                comp.entity!!.addComponent(ChatComponent(mutableListOf("shout")))
                                comp.state = PlayerState.PLAYING
                                comp.writeLine("Welcome to the game, ${event.original}!")
                                comp.prompt = ">"
                            }
                            return true
                        }
                        PlayerState.PLAYING -> return false
                    }
                }
            }
        }
        return false
    }

    private fun newPlayer(game: Game, msg: PlayerConnectEvent): Entity {
        val entity = game.newEntity()
        entity.addComponent(PlayerComponent(msg.player.ip))
        entity.addComponent(CommandComponent())
        return entity
    }
}


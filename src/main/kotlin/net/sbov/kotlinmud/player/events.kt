package net.sbov.kotlinmud.player

import net.sbov.kotlinmud.core.Event

data class PlayerInputEvent(val player: Player, val msg: String): Event
data class PlayerDisconnectEvent(val player: Player): Event
data class PlayerConnectEvent(val player: Player): Event
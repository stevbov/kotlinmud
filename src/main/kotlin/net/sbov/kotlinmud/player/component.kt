package net.sbov.kotlinmud.player

import net.sbov.kotlinmud.core.Component
import net.sbov.kotlinmud.core.Entity
import java.net.SocketAddress

class PlayerComponent(val ip: SocketAddress): Component() {
    var prompt = ""
    var output = ""
    var state = PlayerState.LOGIN

    fun writeLine(str: String) {
        output += str + "\r\n"
    }
}
enum class PlayerState {
    LOGIN, PLAYING
}

fun Entity.player(): PlayerComponent? {
    return component()
}
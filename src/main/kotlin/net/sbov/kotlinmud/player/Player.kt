package net.sbov.kotlinmud.player

import java.net.SocketAddress

interface Player {
    val ip: SocketAddress
    fun disconnect()
    fun write(str: String)
}
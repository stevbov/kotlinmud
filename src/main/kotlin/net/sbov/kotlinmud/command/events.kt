package net.sbov.kotlinmud.command

import net.sbov.kotlinmud.core.Entity
import net.sbov.kotlinmud.core.Event

class CommandEvent(val entity: Entity, input: String): Event {
    val original = input
    val command: String
    val rest: String
    val args: List<String>

    init {
        val splitInput = input.split(Regex(" +"), limit=2)
        command = splitInput[0]

        if (splitInput.size > 1) {
            rest = splitInput[1].trim()
            args = rest.split(Regex(" +"))
        } else {
            rest = ""
            args = listOf()
        }

        System.out.println("Original ${original} command $command rest $rest args $args")
    }
}

class UnhandledCommandEvent(val cmd: CommandEvent): Event

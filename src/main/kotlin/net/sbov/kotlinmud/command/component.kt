package net.sbov.kotlinmud.command

import net.sbov.kotlinmud.core.Component
import net.sbov.kotlinmud.core.Entity
import java.util.concurrent.ConcurrentLinkedQueue

class CommandComponent: Component() {
    val inputs = ConcurrentLinkedQueue<String>()
}

fun Entity.command(): CommandComponent? {
    return component()
}
package net.sbov.kotlinmud.command

import net.sbov.kotlinmud.core.EntitySystem
import net.sbov.kotlinmud.core.Event
import net.sbov.kotlinmud.core.Game

class CommandSystem: EntitySystem(arrayOf()) {
    override fun onUpdate(game: Game) {
        for (entity in game.entities) {
            val comp = entity.command() ?: continue
            val input = comp.inputs.poll()?.trim()

            if (input != null && !input.isBlank()) {
                val event = CommandEvent(entity, input)
                if (!game.sendEvent(event)) {
                    game.sendEvent(UnhandledCommandEvent(event))
                }
            }
        }
    }

    override fun onEvent(game: Game, event: Event): Boolean {
        return false
    }
}
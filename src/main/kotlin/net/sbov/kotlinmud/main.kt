@file:JvmName("Main")
package net.sbov.kotlinmud

import net.sbov.kotlinmud.chat.ChatSystem
import net.sbov.kotlinmud.command.CommandSystem
import net.sbov.kotlinmud.core.Game
import net.sbov.kotlinmud.player.PlayerSystem
import net.sbov.kotlinmud.telnet.TelnetServer
import kotlin.concurrent.thread

fun main(args: Array<String>) {
    val game = Game()
    game.addSystem(CommandSystem())
    game.addSystem(ChatSystem())
    game.addSystem(PlayerSystem())

    val port = 5000
    val server = TelnetServer(port, game)

    thread {
        server.run()
    }

    val gameThread = thread {
        game.run()
    }

    gameThread.join()
}
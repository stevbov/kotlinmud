package net.sbov.kotlinmud.describe

import net.sbov.kotlinmud.core.Component
import net.sbov.kotlinmud.core.Entity

data class DescribeComponent(var name: String): Component()

fun Entity.describe(): DescribeComponent? {
    return component()
}

fun validateName(name: String): String? {
    if (name.length < 3) {
        return "Name must be more than 3 characters long."
    } else if (name.length > 10) {
        return "Name cannot be longer than 10 characters."
    } else if (!name.matches(Regex("[a-zA-Z]+"))) {
        return "Name can only contain letters of the alphabet."
    } else {
        return null
    }
}
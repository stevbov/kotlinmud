package net.sbov.kotlinmud.chat

import net.sbov.kotlinmud.core.ActionEvent
import net.sbov.kotlinmud.core.Entity

class ChatAction(actor: Entity, actorName: String, target: Entity, channel: String, message: String): ActionEvent {
    override val entity = target
    override val message: String
    init {
        if (actor.id == entity.id) {
            this.message = "You $channel '$message'"
        } else {
            this.message = "$actorName ${channel}s '$message'"
        }
    }
}
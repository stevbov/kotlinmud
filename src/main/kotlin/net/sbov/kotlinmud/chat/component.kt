package net.sbov.kotlinmud.chat

import net.sbov.kotlinmud.core.Component
import net.sbov.kotlinmud.core.Entity

class ChatComponent(val channels: MutableList<String> = mutableListOf()): Component()

fun Entity.chat(): ChatComponent? {
    return component()
}

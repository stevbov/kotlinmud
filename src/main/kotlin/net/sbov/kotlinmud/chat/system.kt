package net.sbov.kotlinmud.chat

import net.sbov.kotlinmud.command.CommandEvent
import net.sbov.kotlinmud.core.EntitySystem
import net.sbov.kotlinmud.core.Event
import net.sbov.kotlinmud.core.Game
import net.sbov.kotlinmud.describe.describe
import net.sbov.kotlinmud.player.player

class ChatSystem: EntitySystem() {
    override fun onEvent(game: Game, event: Event): Boolean {
        when (event) {
            is CommandEvent -> {
                val chat = event.entity.chat() ?: return false

                for (channel in chat.channels) {
                    if (channel == event.command) {
                        handleChat(game, channel, event)
                        return true
                    }
                }
            }
        }

        return false
    }

    private fun handleChat(game: Game, channel: String, event: CommandEvent) {
        if (event.rest.isEmpty()) {
            val player = event.entity.player()
            if (player != null) {
                player.writeLine("$channel what?")
            }
            return
        }

        val name = event.entity.describe()?.name ?: "Nameless"

        for (entity in game.entities) {
            val chat = entity.chat() ?: continue
            if (chat.channels.contains(channel)) {
                game.sendEvent(ChatAction(event.entity, name, entity, channel, event.rest))
            }
        }
    }

    override fun onUpdate(game: Game) {
    }
}

package net.sbov.kotlinmud.telnet

import io.netty.channel.ChannelHandlerContext
import net.sbov.kotlinmud.player.Player
import java.net.SocketAddress

class TelnetPlayer(val ctx: ChannelHandlerContext) : Player {
    override val ip: SocketAddress = ctx.channel().remoteAddress()

    override fun disconnect() {
        ctx.close()
    }

    override fun write(str: String) {
        ctx.writeAndFlush(str)
    }
}
package net.sbov.kotlinmud.telnet

import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.ChannelOption
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioServerSocketChannel
import net.sbov.kotlinmud.core.Game
import org.slf4j.LoggerFactory

class TelnetServer(val port: Int, val game: Game) {
    fun run() {
        val bossGroup = NioEventLoopGroup()
        val workerGroup = NioEventLoopGroup()

        try {
            val bootstrap = ServerBootstrap().apply {
                group(bossGroup, workerGroup)
                channel(NioServerSocketChannel::class.java)
                childHandler(TelnetServerChannelInitializer(game))
                option(ChannelOption.SO_BACKLOG, 128)
                childOption(ChannelOption.SO_KEEPALIVE, true)
            }

            val future = bootstrap.bind(port).sync()

            log.info("Telnet player started on port $port")

            future.channel().closeFuture().sync()
        } finally {
            bossGroup.shutdownGracefully()
            workerGroup.shutdownGracefully()
        }
    }

    val log = LoggerFactory.getLogger(this.javaClass)!!
}

package net.sbov.kotlinmud.telnet

import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler
import net.sbov.kotlinmud.core.Game
import net.sbov.kotlinmud.player.Player
import net.sbov.kotlinmud.player.PlayerConnectEvent
import net.sbov.kotlinmud.player.PlayerDisconnectEvent
import net.sbov.kotlinmud.player.PlayerInputEvent
import org.slf4j.LoggerFactory

class TelnetServerHandler(val game: Game) : SimpleChannelInboundHandler<String>() {
    var player: Player? = null

    override fun channelRead0(ctx: ChannelHandlerContext, msg: String) {
        game.queueEvent(PlayerInputEvent(player!!, msg))
    }

    override fun channelInactive(ctx: ChannelHandlerContext) {
        game.queueEvent(PlayerDisconnectEvent(player!!))
        player = null
    }

    override fun channelActive(ctx: ChannelHandlerContext) {
        player = TelnetPlayer(ctx)
        game.queueEvent(PlayerConnectEvent(player!!))
    }

    override fun exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable) {
        log.error("Got exception $cause", cause)
        ctx.close()
        player = null
    }

    val log = LoggerFactory.getLogger(this.javaClass)!!
}

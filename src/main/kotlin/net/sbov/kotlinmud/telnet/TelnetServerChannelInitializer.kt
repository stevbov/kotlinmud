package net.sbov.kotlinmud.telnet

import io.netty.channel.ChannelInitializer
import io.netty.channel.socket.SocketChannel
import io.netty.handler.codec.DelimiterBasedFrameDecoder
import io.netty.handler.codec.Delimiters
import io.netty.handler.codec.string.StringDecoder
import io.netty.handler.codec.string.StringEncoder
import net.sbov.kotlinmud.core.Game

class TelnetServerChannelInitializer(val game: Game) : ChannelInitializer<SocketChannel>() {
    override fun initChannel(ch: SocketChannel) {
        with (ch.pipeline()) {
            addLast("framer", DelimiterBasedFrameDecoder(8192, *Delimiters.lineDelimiter()))
            addLast("decoder", StringDecoder())
            addLast("encoder", StringEncoder())
            addLast(TelnetServerHandler(game))
        }
    }
}